import { readable } from 'svelte/store';

export type InstallPrompt = Event & {
    prompt: () => Promise<void>
}

export default {
    installPrompt: readable(null, set => {
        window.addEventListener('beforeinstallprompt', e => set(e));
    }),
}