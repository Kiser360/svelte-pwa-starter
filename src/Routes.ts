import type { Route } from "./Router/router.service";
import Dashboard from './Views/Dashboard.svelte';

const Routes: Route[] = [
    { path: '/', component: Dashboard },

];

export default Routes;