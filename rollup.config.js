import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import css from 'rollup-plugin-css-only';
import { defineConfig } from 'rollup';
import * as path from 'path';
import replace from '@rollup/plugin-replace';
import workbox from 'rollup-plugin-workbox-inject';

const production = !process.env.ROLLUP_WATCH;

function serve() {
	let server;

	function toExit() {
		if (server) server.kill(0);
	}

	return {
		writeBundle() {
			if (server) return;
			server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
				stdio: ['ignore', 'inherit', 'inherit'],
				shell: true
			});

			process.on('SIGTERM', toExit);
			process.on('exit', toExit);
		}
	};
}

function enableDev() {
	return {
		transform(code, id) {
			if (id.endsWith('main.ts')) {
				return `console.log('Dev Mode Enabled, check window.dev'); window.dev = true;\n${code}`
			}
		}
	}
}

const config = defineConfig([
	{
		input: 'src/main.ts',
		output: {
			sourcemap: true,
			format: 'iife',
			name: 'app',
			file: 'public/build/bundle.js'
		},
		plugins: [
			svelte({
				preprocess: sveltePreprocess({ sourceMap: !production }),
				compilerOptions: {
					// enable run-time checks when not in production
					dev: !production
				}
			}),
			// we'll extract any component CSS out into
			// a separate file - better for performance
			css({ output: 'bundle.css' }),

			// If you have external dependencies installed from
			// npm, you'll most likely need these plugins. In
			// some cases you'll need additional configuration -
			// consult the documentation for details:
			// https://github.com/rollup/plugins/tree/master/packages/commonjs
			resolve({
				browser: true,
				dedupe: ['svelte']
			}),
			commonjs(),
			typescript({
				sourceMap: !production,
				inlineSources: !production
			}),
			!production && enableDev(),

			// In dev mode, call `npm run start` once
			// the bundle has been generated
			!production && serve(),

			// Watch the `public` directory and refresh the
			// browser on changes when not in production
			!production && livereload('public'),

			// If we're building for production (npm run build
			// instead of npm run dev), minify
			production && terser()
		],
		watch: {
			clearScreen: false
		},
		external: production ? [] : [path.resolve(__dirname, 'enableDev.js')]
	},
]);

if (production) {
	config.push({
		input: 'src/sw.js',
		output: {
			sourcemap: false,
			format: 'iife',
			name: 'sw',
			file: 'public/bundle.sw.js',
		},
		plugins: [
			resolve({
				browser: true,
			}),
			replace({
				'process.env.NODE_ENV': JSON.stringify('production'),
				'globalThis.revision': JSON.stringify(Date.now()),
			}),
			workbox({
				globDirectory: 'public',
				globPatterns: [
					'assets/**/*',
					'index.html',
					'**/*.css',
					'build/bundle.js'
				]
			}),
			terser()
		]
	})
}

export default config;
